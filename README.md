GeekData - visualizes technologies, courses and geeks
======================================================

![Universe](https://bitbucket.org/Sufflava/geekdata/raw/master/Images/Root.png)

![JavaScript galaxy](https://bitbucket.org/Sufflava/geekdata/raw/master/Images/JavaScript.png)

![Guru planet](https://bitbucket.org/Sufflava/geekdata/raw/master/Images/CSharpGuru.png)


## OSS Libraries used

GeekData includes source code of the great libraries below for some of its core functionality.
Each library is released under its respective licence:

  - [StacMan](https://github.com/emmettnicholas/StacMan) [(License)](https://github.com/emmettnicholas/StacMan/blob/master/LICENSE)


## Team

 - [FireAlkazar](https://github.com/FireAlkazar) (Pavel Maltsev)
 - [Sufflavus](https://bitbucket.org/Sufflava/) (Tania Shatilova)
 - [GSerjo](https://github.com/GSerjo) (Sergey Morenko)

## Contributing

### To contribute please follow these guidelines:

* Fork the project
* Spaces, not Tabs and no regions
* Make a branch for each thing you want to do
* Send a pull request